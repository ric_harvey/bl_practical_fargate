## Practical Fargate (using CDK & CoPilot)

### Objective

In this lab session, you'll instead use more modern tools to deploy the same chat application. Here you will use CDK to provision the VPC in your account. Then you'll make use of CoPilot CLI to deploy and managed Fargate applications.

### Requirements

- AWS Account
- AWS CLI installed + configured with your API keys
- AWS CDK installed - 1.104.0 or later ([see here](https://github.com/aws/aws-cdk))
- Git CLI installed
- CoPilot CLI installed ([see here](https://github.com/aws/copilot-cli/))

### Expected Time

You should be able to deploy this in 30-45 mins.

### Getting Started

#### Building the VPC

First we need to deploy a VPC for our Fargate applications to live in. If you already have a VPC you might not want to create a new one.

In the repo there is a folder called `infra`. This contains a number of files that declare the AWS "infra"structure that your application needs to run. In real-life scenarios you can declare things like SQS queues, RDS instances or other resources.

It's important to remember that it's good to keep a separate resources for different environments (`dev`, `test`, `prod`). Having the resources declared here makes it easier for developers to know what's deployed. In more advanced use-cases, you would break out resources that are used by multiple services into another repo (this is outside of the scope of this lab).

The business end of the infrastructure is in `infra/lib/chat-app-stack.ts`.

```ts
[...]
    const vpc = new Vpc(this, 'VPC')

    new CfnOutput(this, 'VPCId', {
      exportName: 'VPCId',
      description: 'VPC for the ECS cluster',
      value: vpc.vpcId
    })
[...]
```

We've declared the VPC and we've outputted the `vpcId` so that we can use it in our CoPilot environment.

> **Note** CoPilot does supports creating the VPC (see [here](https://aws.github.io/copilot-cli/docs/commands/env-init/)). Here we want to illustrate how easy it is to add ECS to an existing environment.


##### VPC Creation

To deploy the infrastructure stack using CDK. We want to capture the output `vpcId` into `outputs.json`.

```bash
$ cd infra
$ npm i
$ cdk deploy -O outputs.json
```

_Check the output `cdk deploy` to see what changes will be made to your environment. Choose Yes!_


### Initializing the Chat Application

Now that the VPC has been deployed, we need to boostrap a number of resources to support the ECS cluster. This is done
using the CoPilot CLI.


#### Create the CoPilot Application

An application is a group of related services, environments, and pipelines. Whether you have one service that does everything or a constellation of micro-services, Copilot organizes them and the environments they're deployed to into an "application".

To create our app, we use the CoPilot CLI as follows:

```bash
$ copilot app init chat-app
```

_You should see some resources being configured. CoPilot uses a combination of [SSM parameters](https://console.aws.amazon.com/systems-manager/parameters/?tab=Table) & [CloudFormation stack sets](https://console.aws.amazon.com/cloudformation/home#/stacksets) to maintain the state of the resources in the application._

#### Create the `test` environment

With the app initialized we can create our `test` environment in the VPC we created above.

You should have a `vpcId` show in the output of the previous `cdk deploy` command. You can also grab this from the `output.json` that we piped the variables into.

```bash
$ cat outputs.json
{
  "ChatAppStack": {
    "VPCId": "vpc-0c34b020248b76a3a"
  }
}
```

We initialize the test environment using the following command:

```bash
$ copilot env init -n test --import-vpc-id 'vpc-0c34b020248b76a3a'
Which credentials would you like to use to create test? [profile default]
Which public subnets would you like to use? subnet-0214385529ca14f35, subnet-0be920ef76c7fd77a
Which private subnets would you like to use? subnet-005e2844bee836bb6, subnet-081a941361ee7cfb1
...
```

_You will be prompted to choose which AWS credentials to use, for this lab use the `profile default`. Select **all** of the public and private subnets when prompted._

You can now view the newly created [ECS cluster](https://console.aws.amazon.com/ecs/v2/clusters) and the related [AWS CloudFormation stacks](https://console.aws.amazon.com/cloudformation/home#/stacksets).

You can also inspect the environment using the CLI:

```bash
$ copilot env show -n test
```

#### Deploy the Backend Redis Service

In order to store the state of the chat application, we will deploy a `redis` container as a backend.

> In a real-world scenario you'd most likely use ElasticCache instead to make things easier to manage.

To deploy the backend redis service we do the following:

```bash
# Make sure you are in the root directory (i.e. bl_practical_fargate)
$ copilot svc init --name redis --svc-type "Backend Service" --image redis
```

When you run this command a manifest is generated in the `copilot/` directory. Because there is one included in this repo, it will not be overidden.

Have a look at `copilot/redis/manifest.yaml` and notice that we've provided the `port` that the container exposes. In Fargate the containers get assigned an Elastic Network Interface (ENI) for the entire task so it is not necessary to have this. Nevertheless it's a good practice to include this so other developers are aware of which port the container exposes (i.e. for cases where a custom image is being used).

```yaml
image:
  location: redis

  port: 6379
```

You can see the other parameters such as `cpu` and `memory` too.

To deploy the backend service to the `test` environment we do the following from the root.

```bash
$ copilot svc deploy --name redis --env test
```

_We should see a number of resources being provisioned. CoPilot is taking care of provisioning a lot of things!_

Once complete, you should be able to see a running service in your cluster. You can also use the `show` command:

```bash
$ copilot svc show -n redis
About

  Application       chat-app
  Name              redis
  Type              Backend Service

Configurations

  Environment       Tasks               CPU (vCPU)          Memory (MiB)        Port
  -----------       -----               ----------          ------------        ----
  test              1                   0.25                512                 6379

Service Discovery

  Environment       Namespace
  -----------       ---------
  test              redis.chat-app.local:6379

Variables

  Name                                Container           Environment         Value
  ----                                ---------           -----------         -----
  COPILOT_APPLICATION_NAME            redis               test                chat-app
  COPILOT_ENVIRONMENT_NAME              "                   "                 test
  COPILOT_SERVICE_DISCOVERY_ENDPOINT    "                   "                 chat-app.local
  COPILOT_SERVICE_NAME                  "                   "                 redis
```

We can see that service discovery has already been setup for this service. To access the redis container we can use the `redis.chat-app.local` domain.


#### Deploy the Frontend Chat Service

Now that we have the backend service running, we can deploy the frontend. We follow similar steps to the `redis` case except we don't provide an image but instead we point to a Dockerfile.

```bash
$ copilot svc init --name frontend
Service type: Load Balanced Web Service
Dockerfile: fargate_app/Dockerfile
[...]
```

Before deploying, let's inspect the `copilot/frontend/manifest.yaml`. Notice that we have provided the `REDIS_ENDPOINT` environment variable with the domain of the redis service.

```yaml
variables:                    # Pass environment variables as key value pairs.
  REDIS_ENDPOINT: redis.chat-app.local
```

If you check `fargate_app/index.js` then you can see we pass the environment variable into the application as a configuration. It's important that when you build container images, you provide a way to customize the configuration from outside the app like this. When we move the image to a `prod` environment we will supply a different endpoint. For more details on this [see here](https://aws.github.io/copilot-cli/docs/developing/environment-variables/).

```js
io.adapter(redis({ host: process.env.REDIS_ENDPOINT , port: 6379 }));
```

So let's deploy this application to our test environment. We use the deploy command as follows:

```bash
$ copilot svc deploy --name frontend --env test
```

_When we do this, CoPilot will build and push the container image to ECR for us. Then it will provision all the AWS resources for our service_


### Testing it Out

Now that our app is deployed we can test it out. We can get the URL for the external load balancer via the output of the previous command or via:

```bash
$ copilot svc show -n frontend
```

Navigate to the URL, you should be able to enter your name and start chatting away.

![Chat app screenshot 1](./../img/screenshot1.png)
![Chat app screenshot 2](./../img/screenshot2.png)

### Summary

You've now deployed two services to Fargate using the CoPilot CLI. One of the services is an external facing `frontend` service. The other is a backend `redis` service that is only accessible from within the VPC. When `frontend` communicates with `redis` it makes use of service discovery.


### Extras - Logs (Optional)

You can view logs for your applications using the CoPilot CLI. Try view the logs of the Redis service:

```bash
$ copilot svc logs -n redis
```

### Extras - Exec (Optional)

You can exec into a Fargate task using the `copilot svc exec` option. Try connecting to a `frontend` task:

```bash
$ copilot svc exec -n frontend
Execute `/bin/sh` in container frontend in task 301ddf0bb262480394dca9d0c3939c37.

Starting session with SessionId: ecs-execute-command-04f4d75ac73b1b9b4
# exit
```

### Clean Up

To clean up we first need to delete our CoPilot Application. We can use the following command:

```bash
$ copilot app delete
```

_This should delete both services and the test environment_

We can delete the CDK resources by going to the `infra/` directory and running the following:

```bash
$ cd infra
$ cdk destroy
```

Once that's done - that's it!!

### What next? (Optional Challenges)

Here are a few things you can try if you want to take it to another level:

- Try deploy a separate task that runs every day and connects to redis, performs a `BGSAVE` operation and save the results to S3.
- Try configure auto scaling on the frontend service to help handle peak traffic spikes.
- Try configure a build pipeline for your image using [copilot pipeline init](https://aws.github.io/copilot-cli/docs/concepts/pipelines/)
