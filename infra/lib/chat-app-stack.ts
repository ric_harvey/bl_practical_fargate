import { Vpc } from '@aws-cdk/aws-ec2';
import * as cdk from '@aws-cdk/core';
import { CfnOutput } from '@aws-cdk/core';

export class ChatAppStack extends cdk.Stack {
  constructor(scope: cdk.Construct, id: string, props?: cdk.StackProps) {
    super(scope, id, props);

    // Automatically creates public, private subnets and sets up NAT gateway.
    const vpc = new Vpc(this, 'VPC')

    new CfnOutput(this, 'VPCId', {
      exportName: 'VPCId',
      description: 'VPC for the ECS cluster',
      value: vpc.vpcId
    })
  }
}
